package controller;

import api.IChicago;
import model.logic.Chicago;

public class Controlador {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Instancia del api
	 */
	private static IChicago chicago = new Chicago();

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos -----------------------------------------------
	// ------------------------------------------------------------------------------------
	
	/**
	 * Carga de archivos.
	 * @param rutaJson
	 * @throws Exception si hay problemas en el m�todo.
	 */
	public static void cargar(String rutaJson) throws Exception { 
		chicago.cargar(rutaJson);
	}

	/**
	 * Retorna cantidad total de taxis en el �rbol.
	 * @return cantidadTaxis
	 */
	public static int getCantidadTaxis() { 
		return chicago.getCantidadTaxis();
	}

	/**
	 * Retorna la altura del �rbol.
	 * @return alturaArbol
	 */
	public static int getAlturaArbol() { 
		return chicago.getAlturaArbol();
	}

	/**
	 * Retorna la altura promedio de los nodos del �rbol.
	 * @return alturaPromedio
	 */
	public static double getAlturaPromedio() {
		return chicago.getAlturaPromedio();
	}

	/**
	 * Retorna la informaci�n de un taxi seg�n su id.
	 * @param idTaxi
	 * @return informacionTaxi
	 */
	public static String getInformacionTaxi(String idTaxi) {
		return chicago.getInformacionTaxi(idTaxi);
	}

	/**
	 * Retorna los id de los taxis entre un rango de id.
	 * @param llaveSuperior
	 * @param llaveInferior
	 * @return idsTaxis
	 */
	public static Iterable<String> getKeysInRange(String llaveInferior, String llaveSuperior) {
		return chicago.getKeysInRange(llaveInferior, llaveSuperior);
	}
	
}
