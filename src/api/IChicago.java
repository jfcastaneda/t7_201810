package api;

public interface IChicago {

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Carga de archivos.
	 * @param rutaJson
	 * @throws Exception si hay problemas en el m�todo.
	 */
	public void cargar(String rutaJson) throws Exception;

	/**
	 * Retorna cantidad total de taxis en el �rbol.
	 * @return cantidadTaxis
	 */
	public int getCantidadTaxis();

	/**
	 * Retorna la altura del �rbol.
	 * @return alturaArbol
	 */
	public int getAlturaArbol();

	/**
	 * Retorna la altura promedio de los nodos del �rbol.
	 * @return alturaPromedio
	 */
	public double getAlturaPromedio();

	/**
	 * Retorna la informaci�n de un taxi seg�n su id.
	 * @param idTaxi
	 * @return informacionTaxi
	 */
	public String getInformacionTaxi(String idTaxi);

	/**
	 * Retorna los id de los taxis entre un rango de id.
	 * @param llaveSuperior
	 * @param llaveInferior
	 * @return idsTaxis
	 */
	public Iterable<String> getKeysInRange(String llaveInferior, String llaveSuperior);
}
