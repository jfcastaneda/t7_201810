package model.logic;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import api.IChicago;
import model.data_structures.IRedBlackBST;
import model.data_structures.RedBlackBST;
import model.vo.VOTaxi;
import utils.ServicioDeCarga;

public class Chicago implements IChicago {

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Direcci�n del archivo Json peque�o.
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	/**
	 * Direcci�n del archivo Json mediano.
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";

	/**
	 * Direcci�n del archivo Json grande.
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * �rbol rojo negro de taxis.
	 */
	private IRedBlackBST<String, VOTaxi> taxis;
	
	/**
	 * Altura promedio del �rbol.
	 */
	private double alturaPromedio;

	// ------------------------------------------------------------------------------------
	// ------------------------- Constructor -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Constructor.
	 */
	public Chicago() {
		alturaPromedio = 0;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Carga de archivos.
	 * @param rutaJson
	 * @throws Exception si hay problemas en el m�todo.
	 */
	public void cargar(String rutaJson) throws Exception {

		taxis = new RedBlackBST<>();
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(rutaJson));
		Type collectionType = new TypeToken<Collection<ServicioDeCarga>>(){}.getType();
		Collection<ServicioDeCarga> subData = gson.fromJson(reader, collectionType);
		Iterator<ServicioDeCarga> data = subData.iterator();

		VOTaxi taxi;
		ServicioDeCarga servicio;
		while(data.hasNext()) {
			servicio = data.next();
			taxi = new VOTaxi(servicio.getCompany());
			if(!taxis.contains(servicio.getTaxi_id())) {
				taxis.put(servicio.getTaxi_id(), taxi);
			}
			taxi = taxis.get(servicio.getTaxi_id());
			taxi.agregarServicio(servicio);
		}
	}
	
	/**
	 * Retorna cantidad total de taxis en el �rbol.
	 * @return cantidadTaxis
	 */
	public int getCantidadTaxis() {
		return taxis.size();
	}
	
	/**
	 * Retorna la altura del �rbol.
	 * @return alturaArbol
	 */
	public int getAlturaArbol() {
		return taxis.height();
	}
	
	/**
	 * Retorna la altura promedio de los nodos del �rbol.
	 * @return alturaPromedio
	 */
	public double getAlturaPromedio() {
		if(alturaPromedio == 0 && taxis.size() != 0) {
			Iterator<String> keys = taxis.keys().iterator();
			while(keys.hasNext()) {
				alturaPromedio += taxis.getHeight(keys.next());
			}
			alturaPromedio = alturaPromedio / taxis.size();
		}
		return alturaPromedio;
	}
	
	/**
	 * Retorna la informaci�n de un taxi seg�n su id.
	 * @param idTaxi
	 * @return informacionTaxi
	 */
	public String getInformacionTaxi(String idTaxi) {
		return taxis.get(idTaxi).toString();
	}
	
	/**
	 * Retorna los id de los taxis entre un rango de id.
	 * @param llaveSuperior
	 * @param llaveInferior
	 * @return idsTaxis
	 */
	public Iterable<String> getKeysInRange(String llaveInferior, String llaveSuperior) {
		return taxis.keysInRange(llaveInferior, llaveSuperior);
	}
	
}
