package model.data_structures;

public interface IRedBlackBST <K, V> {

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Retorna el numero de parejas [llave,Valor] del arbol.
	 */
	public int size();

	/**
	 * Informa si el arbol es vacio.
	 */
	public boolean isEmpty();

	/**
	 * Retorna el valor asociado a una llave dada. Si la llave no se encuentra retorna el valor null.
	 */
	public V get(K key);

	/**
	 * Retorna la altura del camino desde la raiz para llegar a la llave key (si la llave existe). 
	 * Retorna valor -1 si la llave no existe.
	 */
	public int getHeight(K key);

	/**
	 * Indica si la llave key se encuentra en el arbol.
	 */
	public boolean contains(K key);

	/**
	 * Inserta la pareja [key, val] en el arbol. Si la llave ya existe se reemplaza el valor.
	 * El valo val no puede ser null. Si el valor asociado es null se debe lanzar una Exception.
	 */
	public void put(K key, V val);

	/**
	 * Retorna la altura del arbol.
	 */
	public int height();

	/**
	 * Retorna la llave mas pequena del arbol. Valor null si arbol vacio.
	 */
	public K min();

	/**
	 * Retorna la llave mas grande del arbol. Valor null si arbol vacio.
	 */
	public K max();

	/**
	 * Retorna todas las llaves del arbol como un iterator.
	 */
	public Iterable<K> keys();

	//	/**
	//	 * Retorna todos los valores del arbol que se encuentran en el rango de llaves.
	//	 */
	//	public Iterable<V> valuesInRange(K init, K end);
	// TODO: Implementar el metodo values

	/**
	 * Retorna todas las llaves del arbol que se encuentran en el rango de llaves dado.
	 */
	public Iterable<K> keysInRange(K init, K end);
	
	
	public Iterable<V> ValuesInRange(K init, K end);	
}
