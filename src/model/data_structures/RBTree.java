package model.data_structures;

import java.util.Comparator;

import java.math.*;

public class RBTree<K extends Comparable<K>,V> implements IRedBlackBST<K, V>{

	public static class Nodo<K,V>{


		private K llave;

		private V valor;

		private Nodo<K, V> derecha;

		private Nodo<K, V> izquierda;

		public final static boolean ROJO = true;

		public final static boolean NEGRO = !ROJO;
		//True rojo, false negro.
		private boolean color;

		private int altura;


		public Nodo(K llave, V valor, int altura){
			this.derecha = null;
			this.izquierda = null;
			this.llave = llave;
			this.valor = valor;
			this.altura = altura;
			color = ROJO;
		}

		public void setDerecha(Nodo<K, V> derecha) {
			this.derecha = derecha;
		}

		public void setIzquierda(Nodo<K, V> izquierda) {
			this.izquierda = izquierda;
		}

		public boolean esRojo() {
			return color==ROJO;
		}

		public void setColor(boolean color) {
			this.color = color;
		}


		public K getLlave() {
			return llave;
		}

		public void setLlave(K llave) {
			this.llave = llave;
		}

		public V getValor() {
			return valor;
		}

		public void setValor(V valor) {
			this.valor = valor;
		}

		public Nodo<K, V> getDerecha() {
			return derecha;
		}

		public Nodo<K, V> getIzquierda() {
			return izquierda;
		}

		public int darAltura(){
			return altura;
		}

		//		public ArrayList<K> darLlavesPreOrden(ArrayList<K> lista){
		//			lista.add(this.llave);
		//			if(izquierda!=null) izquierda.darLlavesPreOrden(lista);
		//			if(derecha!=null) derecha.darLlavesPreOrden(lista);
		//			return lista;
		//		}
		//		
		//		public ArrayList<V> darValoresPreOrden(ArrayList<V> lista){
		//			lista.add(this.valor);
		//			if(izquierda!=null) izquierda.darValoresPreOrden(lista);
		//			if(derecha!=null) derecha.darValoresPreOrden(lista);
		//			return lista;
		//		}
		//		
		//		public ArrayList<K> darLlavesPostOrden(ArrayList<K> lista){
		//			if(izquierda!=null) izquierda.darLlavesPostOrden(lista);
		//			if(derecha!=null) derecha.darLlavesPostOrden(lista);
		//			lista.add(this.llave);
		//			return lista;
		//		}
		//		
		//		public ArrayList<V> darValoresPostorden(ArrayList<V> lista){
		//			if(izquierda!=null) izquierda.darValoresPostorden(lista);
		//			if(derecha!=null) derecha.darValoresPostorden(lista);
		//			lista.add(this.valor);
		//			return lista;
		//		}

	}

	private Nodo<K, V> raiz;

	private Comparator<K> comparador;

	private int size;

	private int altura;

	public RBTree(Comparator<K> comparador)
	{
		raiz=null;
		this.comparador=comparador;

	}

	public RBTree()
	{
		this(null);
	}

	public Nodo<K,V> getNodoRaiz(
			){
		return raiz;
	}


	@Override
	public int size() {
		if(raiz==null) return 0;
		else return size = calcularTamanio(raiz);
	}

	public V get(K llave){
		return get(raiz,llave);
	}

	private V get(Nodo<K, V> Nodo, K llave){
		if(Nodo==null) 
			return null;
		int com = (comparador!=null)?comparador.compare(Nodo.llave,llave):Nodo.llave.compareTo(llave);
		if(com<0) 
			return get(Nodo.derecha, llave);
		else if(com>0) 
			return get(Nodo.izquierda,llave);
		else
			return Nodo.valor;
	}

	@Override
	public boolean isEmpty(){		// TODO Auto-generated method stub
		return raiz==null;
	}

	@Override
	public boolean contains(K key){
		return contains(raiz,key);
	}

	private boolean contains(Nodo<K, V> Nodo, K llave){
		if(Nodo==null) return false;
		int com = (comparador!=null)?comparador.compare(Nodo.llave,llave):Nodo.llave.compareTo(llave);
		if(com<0) 
			return contains(Nodo.derecha, llave);
		else if(com>0) 
			return contains(Nodo.izquierda,llave);
		else 
			return true;
	}

	@Override
	public int height() {
		return calcularAltura(raiz);
	}

	@Override
	public int getHeight(K key){
		System.out.println(key);
		Nodo<K,V> Nodo = getNodo(key);
		if(Nodo!=null) { 
			System.out.println("mierda");
			return -1;
		}
		return calcularAltura(Nodo);	
	}

	private int calcularAltura(Nodo<K, V> Nodo){
		if (Nodo == null) 
			return -1;
		else
			return 1 + Math.max(calcularAltura(Nodo.izquierda), calcularAltura(Nodo.derecha));
	}

	public Iterable<K> keysInRange(K lo, K hi) {
		if (lo == null || hi==null) 
			return null;
		Queue<K> queue = new Queue<K>();
		keysInRange(raiz, queue, lo, hi);
		return queue;
	}

	private void keysInRange(Nodo<K,V> x, Queue<K> queue, K lo, K hi){ 
		if (x == null) 
			return; 
		int cmplo = (comparador!=null)?comparador.compare(lo, x.llave):lo.compareTo(x.llave); 
		int cmphi = (comparador!=null)?comparador.compare(hi, x.llave):hi.compareTo(x.llave);
		if (cmplo < 0)
			keysInRange(x.izquierda, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0)
			queue.enqueue(x.llave); 
		if (cmphi > 0)
			keysInRange(x.derecha, queue, lo, hi); 
	}

	@Override
	public Iterable<K> keys(){
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<V> ValuesInRange(K init, K end) {
		Queue<V> ans = new Queue<V>();

		Iterable<K> llaves = keysInRange(init, end);
		for (K k : llaves) {
			ans.enqueue(get(k));
		}
		return ans;
	}

	public void put(K llave, V valor){
		if(llave!=null) {
			raiz = put(llave, valor, raiz);
			raiz.color = Nodo.NEGRO;
		}
	}

	private Nodo<K, V> put(K llave, V valor, Nodo<K,V> Nodo){
		if(Nodo==null) 
			return new Nodo<K,V>(llave, valor,0);
		int com = (comparador!=null)? comparador.compare(Nodo.llave, llave):Nodo.llave.compareTo(llave);
		if(com<0) 
			Nodo.derecha = put(llave, valor, Nodo.derecha);
		else if(com>0) 
			Nodo.izquierda = put(llave, valor, Nodo.izquierda);
		else Nodo.valor = valor;
		if(derechaRoja(Nodo))  
			Nodo = rotarIzquierda(Nodo);
		if(dosRojosEnLaIzquierda(Nodo)) 
			Nodo = rotarDerecha(Nodo);
		if(dosHijosRojos(Nodo)) 
			invertirColores(Nodo);
		Nodo.altura = calcularAltura(Nodo.izquierda)+calcularAltura(Nodo.derecha)+1;
		return Nodo;

	}

	public void delete(K llave) {
		delete(raiz, llave);
		if(raiz!=null) raiz.color=Nodo.NEGRO;
	}

	private void delete(Nodo<K,V> Nodo, K llave) {
		if(Nodo!=null) {
			Nodo<K, V> derecho = Nodo.derecha;
			Nodo<K, V> izquierdo = Nodo.izquierda;
			int com = (comparador!=null)?comparador.compare(Nodo.llave, llave):Nodo.llave.compareTo(llave);
			//RAMA IZQUIERDA
			if(com>0 && izquierdo!=null) {
				if(izquierdo.getLlave().equals(llave)) {
					//Caso 1: No tiene hijos
					if(izquierdo.derecha==null && izquierdo.izquierda==null) {
						Nodo.setIzquierda(null);
					}
					//Caso 2: Solo tiene uno de los dos hijos
					else if(izquierdo.derecha!=null && izquierdo.izquierda==null) {	
						Nodo<K, V> aux= izquierdo.derecha;
						izquierdo.setColor(aux.color);
						izquierdo.setValor(aux.valor);
						izquierdo.setLlave(aux.llave);
						izquierdo.derecha = aux.derecha;
						izquierdo.izquierda = aux.izquierda;
					}
					else if(izquierdo.derecha==null && izquierdo.izquierda!=null) {	
						Nodo<K, V> aux= izquierdo.izquierda;
						izquierdo.setColor(aux.color);
						izquierdo.setValor(aux.valor);
						izquierdo.setLlave(aux.llave);
						izquierdo.derecha = aux.derecha;
						izquierdo.izquierda = aux.izquierda;
					}
					//Caso 3: Tiene dos hijos
					else if (izquierdo.derecha!=null && izquierdo.derecha!=null)
					{
						boolean comp = calcularTamanio(izquierdo.derecha)>=calcularTamanio(izquierdo.izquierda);
						if(!comp) 
						{
							Nodo<K,V> aux = darMayor(izquierdo.izquierda);
							Nodo<K, V> padreAux = buscarPadre(aux);
							izquierdo.llave = aux.llave;
							izquierdo.valor = aux.valor;
							if(padreAux.derecha!=null)
							{
								if(padreAux.derecha.llave.equals(aux.llave))
								{
									padreAux.derecha=null;
									if(padreAux.color==Nodo.ROJO)
									{
										padreAux.izquierda.color = Nodo.ROJO;
										padreAux.color= Nodo.NEGRO;
									}
								}
							}
							else if(padreAux.izquierda!=null)
							{
								if(padreAux.izquierda.llave.equals(aux.llave))
								{
									padreAux.izquierda = null;
									if(padreAux.derecha!=null)
									{
										Nodo<K, V> aux2=aux.derecha;
										padreAux.izquierda=aux2;
										padreAux.izquierda.valor=padreAux.valor;
										padreAux.izquierda.llave=padreAux.llave;
										padreAux.llave=aux2.llave;
										padreAux.valor=aux2.valor;
										padreAux.derecha=null;

									}
									else padreAux.color=Nodo.ROJO;
								}
							}
						}
						else if (comp) 
						{
							Nodo<K, V> aux=darMenor(izquierdo.derecha);
							Nodo<K, V> padreAux=buscarPadre(aux);
							izquierdo.valor= aux.valor;
							izquierdo.llave = aux.llave;
							if(padreAux.derecha!=null)
							{
								if(padreAux.derecha.llave.equals(aux.llave))
								{
									padreAux.derecha=null;
									if(padreAux.color==Nodo.ROJO)
									{
										padreAux.izquierda.color = Nodo.ROJO;
										padreAux.color= Nodo.NEGRO;
									}
								}
							}
							else if(padreAux.izquierda!=null)
							{
								if(padreAux.izquierda.llave.equals(aux.llave))
								{
									padreAux.izquierda = null;
									if(padreAux.derecha!=null)
									{
										Nodo<K, V> aux2=aux.derecha;
										padreAux.izquierda=aux2;
										padreAux.izquierda.valor=padreAux.valor;
										padreAux.izquierda.llave=padreAux.llave;
										padreAux.llave=aux2.llave;
										padreAux.valor=aux2.valor;
										padreAux.derecha=null;

									}
									else padreAux.color=Nodo.ROJO;
								}
							}
						}
					}
				}
				else delete(izquierdo, llave);
				//RAMA DERECHA
			}
			else if(com<0 && derecho!=null) 
			{
				if(derecho.getLlave().equals(llave))
				{
					//Caso 1: No tiene hijos
					if(derecho.derecha==null && derecho.izquierda==null)
					{
						Nodo.setIzquierda(null);
					}
					//Caso 2: Solo tiene uno de los dos hijos
					else if(derecho.derecha!=null && derecho.izquierda==null)
					{	
						Nodo<K, V> aux= derecho.derecha;
						derecho.setColor(aux.color);
						derecho.setValor(aux.valor);
						derecho.setLlave(aux.llave);
						derecho.derecha = aux.derecha;
						derecho.izquierda = aux.izquierda;
					}
					else if(derecho.derecha==null && derecho.izquierda!=null) 
					{	
						Nodo<K, V> aux= derecho.izquierda;
						derecho.setColor(aux.color);
						derecho.setValor(aux.valor);
						derecho.setLlave(aux.llave);
						derecho.derecha = aux.derecha;
						derecho.izquierda = aux.izquierda;
					}
					//Caso 3: Tiene dos hijos
					else if (derecho.derecha!=null && derecho.derecha!=null) 
					{
						boolean comp = calcularTamanio(derecho.derecha)>=calcularTamanio(derecho.izquierda);
						if(!comp) 
						{
							Nodo<K,V> aux = darMayor(derecho.izquierda);
							Nodo<K, V> padreAux = buscarPadre(aux);
							izquierdo.llave = aux.llave;
							izquierdo.valor = aux.valor;
							if(padreAux.derecha!=null)
							{
								if(padreAux.derecha.llave.equals(aux.llave))
								{
									padreAux.derecha=null;
									if(padreAux.color==Nodo.ROJO)
									{
										padreAux.izquierda.color = Nodo.ROJO;
										padreAux.color= Nodo.NEGRO;
									}
								}
							}
							else if(padreAux.izquierda!=null)
							{
								if(padreAux.izquierda.llave.equals(aux.llave))
								{
									padreAux.izquierda = null;
									if(padreAux.derecha!=null)
									{
										Nodo<K, V> aux2=aux.derecha;
										padreAux.izquierda=aux2;
										padreAux.izquierda.valor=padreAux.valor;
										padreAux.izquierda.llave=padreAux.llave;
										padreAux.llave=aux2.llave;
										padreAux.valor=aux2.valor;
										padreAux.derecha=null;

									}
									else padreAux.color=Nodo.ROJO;
								}
							}
						}
						else if (comp) 
						{
							Nodo<K, V> aux=darMenor(izquierdo.derecha);
							Nodo<K, V> padreAux=buscarPadre(aux);
							izquierdo.valor= aux.valor;
							izquierdo.llave = aux.llave;
							if(padreAux.derecha!=null)
							{
								if(padreAux.derecha.llave.equals(aux.llave))
								{
									padreAux.derecha=null;
									if(padreAux.color==Nodo.ROJO)
									{
										padreAux.izquierda.color = Nodo.ROJO;
										padreAux.color= Nodo.NEGRO;
									}
								}
							}
							else if(padreAux.izquierda!=null)
							{
								if(padreAux.izquierda.llave.equals(aux.llave))
								{
									padreAux.izquierda = null;
									if(padreAux.derecha!=null)
									{
										Nodo<K, V> aux2=aux.derecha;
										padreAux.izquierda=aux2;
										padreAux.izquierda.valor=padreAux.valor;
										padreAux.izquierda.llave=padreAux.llave;
										padreAux.llave=aux2.llave;
										padreAux.valor=aux2.valor;
										padreAux.derecha=null;

									}
									else padreAux.color=Nodo.ROJO;
								}
							}
						}
					}
				}
				else delete(derecho, llave);
			}
			else if (com==0) {

				if(derecho!=null)
				{
					Nodo<K, V> aux=darMenor(derecho);
					aux.derecha = raiz.derecha;
					aux.izquierda = raiz.izquierda;
					raiz=aux;
				}
				else if(izquierdo!=null)
				{
					Nodo<K, V> aux=darMayor(izquierdo);
					aux.derecha = raiz.derecha;
					aux.izquierda = raiz.izquierda;
					raiz=aux;				}
				else
				{
					raiz=null;
				}
			}
		}
	}

	public Nodo<K,V> getNodo(K llave){
		return getNodo(raiz,llave);
	}

	private Nodo<K,V> getNodo(Nodo<K, V> Nodo, K llave){
		if(Nodo==null) 
			return null;
		int com = (comparador!=null)?comparador.compare(Nodo.llave,llave):Nodo.llave.compareTo(llave);
		System.out.println(com);
		if(com<0) 
			return getNodo(Nodo.derecha, llave);
		else if(com>0) 
			return getNodo(Nodo.izquierda,llave);
		else 
			return Nodo;
	}


	public Nodo<K,V> buscarPadre(Nodo<K, V> Nodo) {
		return buscarPadre(raiz, Nodo);
	}

	private Nodo<K,V> buscarPadre(Nodo<K, V> NodoActual, Nodo<K, V> NodoBusqueda) {
		Nodo<K, V> rta = null;
		if(NodoActual==null||NodoBusqueda==null) 
			return rta;
		int comp = (comparador!=null)?comparador.compare(NodoActual.llave,NodoBusqueda.llave):NodoActual.llave.compareTo(NodoBusqueda.llave);
		if(comp==0) 
			return null;
		else if (comp>0) 
		{
			if(NodoActual.izquierda.equals(NodoBusqueda.llave)) 
				return NodoActual;
			else 
				return buscarPadre(NodoActual.izquierda, NodoBusqueda);
		}
		else if(comp<0) 
		{
			if(NodoActual.derecha.equals(NodoBusqueda.llave)) 
				return NodoActual;
			else 
				return buscarPadre(NodoActual.derecha, NodoBusqueda);
		}
		return rta;
	}

	public K min(){
		return darMenor(raiz).llave;
	}

	private Nodo<K,V> darMenor(Nodo<K, V> Nodo) {
		if(Nodo==null) 
			return null;
		if(Nodo.izquierda!=null) 
			return darMenor(Nodo.izquierda);
		else if (Nodo.derecha!=null)
			return darMenor(Nodo.derecha);
		return Nodo;
	}

	public Nodo<K,V> maxNode(){
		return darMayor(raiz);
	}

	public K max(){
		return darMayor(raiz).llave;
	}



	private Nodo<K,V> darMayor(Nodo<K, V> Nodo) {
		if(Nodo==null) 
			return null;
		if(Nodo.izquierda!=null) 
			return darMayor(Nodo.izquierda);
		else if (Nodo.derecha!=null) 
			return darMayor(Nodo.derecha);
		else
			return Nodo;
	}

	//	public ArrayList<K> darLlavesPreorden(){
	//		ArrayList<K> ans = new ArrayList<K>();
	//		raiz.darLlavesPreOrden(ans);
	//		return ans;
	//	}
	//	
	//	public ArrayList<V> darValoresPreorden(){
	//		ArrayList<V> ans = new ArrayList<V>();
	//		raiz.darValoresPreOrden(ans);
	//		return ans;
	//	}
	//	
	//	public ArrayList<K> darLlavesPostOrden(){
	//		ArrayList<K> ans = new ArrayList<K>();
	//		raiz.darLlavesPostOrden(ans);
	//		return ans;
	//	}
	//	
	//	public ArrayList<V> darValoresPostOrden(){
	//		ArrayList<V> ans = new ArrayList<V>();
	//		raiz.darValoresPostorden(ans);
	//		return ans;
	//	}



	//------------------------------------------------------------------------------------------------
	//METODOS PARA VERIFICAR QUE EL ARBOL CUMPLA LAS REGLAS
	//------------------------------------------------------------------------------------------------

	
	private boolean esRojo(Nodo<K, V> Nodo) 
	{
		return (Nodo!=null)? Nodo.ROJO==Nodo.color:false;
	}

	//Retorna true si se incumple con la regla, false en el ccaso contrario
	private boolean derechaRoja(Nodo<K,V> Nodo) 
	{
		return esRojo(Nodo.derecha) && !esRojo(Nodo.izquierda);
	}

	//Retorna true si se incumple con la regla, false en el ccaso contrario
	private boolean dosRojosEnLaIzquierda(Nodo<K, V> Nodo)
	{
		return esRojo(Nodo.izquierda) && esRojo(Nodo.izquierda.izquierda);
	}

	//Retorna true si se incumple con la regla, false en el ccaso contrario
	private boolean dosHijosRojos(Nodo<K, V> Nodo) 
	{
		return esRojo(Nodo.derecha) && esRojo(Nodo.izquierda);
	}

	//------------------------------------------------------------------------------------------------
	//METODOS PARA BALANCEAR EL ARBOL
	//------------------------------------------------------------------------------------------------

	private Nodo<K, V> rotarDerecha(Nodo<K, V> Nodo)
	{
		Nodo<K, V> temp = Nodo.izquierda;
		Nodo.izquierda= temp.derecha;
		temp.derecha = Nodo;
		temp.color = temp.derecha.color;
		temp.derecha.color=Nodo.ROJO;
		return temp;
	}

	private Nodo<K, V> rotarIzquierda(Nodo<K, V>Nodo)
	{
		Nodo<K, V> temp = Nodo.derecha;
		Nodo.derecha= temp.izquierda;
		temp.izquierda = Nodo;
		temp.color = temp.izquierda.color;
		temp.izquierda.color=Nodo.ROJO;
		return temp;
	}

	private void invertirColores(Nodo<K, V> Nodo)
	{
		Nodo.color = !Nodo.color;
		Nodo.izquierda.color = !Nodo.izquierda.color; 
		Nodo.derecha.color = !Nodo.derecha.color; 

	}

	private int calcularTamanio(Nodo<K, V> Nodo)
	{
		int rta = 1;
		if(Nodo==null) 
			return rta;
		else {
			if(Nodo.izquierda!=null) 
				rta+=calcularTamanio(Nodo.izquierda);
			if(Nodo.derecha!=null) 
				rta+=calcularTamanio(Nodo.derecha);
			return rta;
		}
	}

}