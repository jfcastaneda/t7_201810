package model.vo;

import utils.ServicioDeCarga;

public class VOTaxi {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Compa��a del taxi.
	 */
	private String compania;

	/**
	 * N�mero de servicios realizados.
	 */
	private int cantidadServicios;

	/**
	 * Total dinero ganado.
	 */
	private double gananciaTotal;

	/**
	 * Total duraci�n de los servicios.
	 */
	private long duracionTotal;

	/**
	 * Total distancia recorrida.
	 */
	private double distanciaRecorridaTotal;

	// ------------------------------------------------------------------------------------
	// ------------------------- Constructor -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Constructor
	 * @param compania
	 */
	public VOTaxi(String compania) {
		super();
		this.compania = compania;
		this.cantidadServicios = 0;
		this.gananciaTotal = 0;
		this.duracionTotal = 0;
		this.distanciaRecorridaTotal = 0;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Retorna la compa��a.
	 * @return compania
	 */
	public String getCompania() {
		return compania;
	}

	/**
	 * Retorna la cantidad de servicios.
	 * @return cantidadServicios
	 */
	public int getCantidadServicios() {
		return cantidadServicios;
	}

	/**
	 * Retorna la ganancia total.
	 * @return gananciaTotal
	 */
	public double getGananciaTotal() {
		return gananciaTotal;
	}

	/**
	 * Retorna la duraci�n total.
	 * @return duracionTotal
	 */
	public long getDuracionTotal() {
		return duracionTotal;
	}

	/**
	 * Retorna la distancia recorrida total.
	 * @return distanciaRecorridaTotal
	 */
	public double getDistanciaRecorridaTotal() {
		return distanciaRecorridaTotal;
	}
	
	/**
	 * Agrega un servicio a la cuenta.
	 */
	public void agregarServicio(ServicioDeCarga servicio) {
		cantidadServicios++;
		gananciaTotal += servicio.getTrip_total();
		duracionTotal += servicio.getTrip_seconds();
		distanciaRecorridaTotal += servicio.getTrip_miles();
	}
	
	/**
	 * To String.
	 * @return toString
	 */
	@Override
	public String toString() {
		return "El taxi con dicho Id tiene como compa��a: " + compania
				+ ", ha generado una ganancia total por: " + gananciaTotal
				+ ", ha recorrido un total de " + distanciaRecorridaTotal + " millas"
				+ " durante " + duracionTotal + " segundos.";
	}
}
