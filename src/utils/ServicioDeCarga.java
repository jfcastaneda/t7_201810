package utils;

public class ServicioDeCarga {

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Compa��a por defecto en caso que no exista.
	 */
	public final static String NO_COMPANY = "Independent Owner";

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Id del taxi que realiz� el servicio.
	 */
	private String taxi_id;

	/**
	 * Compa��a del taxi que realiz� el servicio.
	 */
	private String company;

	/**
	 * Dinero que se pag� por el servicio.
	 */
	private double trip_total;

	/**
	 * Segundos de duraci�n del servicio.
	 */
	private int trip_seconds;

	/**
	 * Distancia recorrida durante el servicio.
	 */
	private double trip_miles;

	// ------------------------------------------------------------------------------------
	// ------------------------- Constructor -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Constructor del servicio del cual se extraer�n los datos.
	 * @param taxi_id
	 * @param company
	 * @param trip_total
	 * @param trip_seconds
	 * @param trip_miles
	 */
	public ServicioDeCarga(String taxi_id, String company, double trip_total, int trip_seconds, double trip_miles) {
		this.taxi_id = taxi_id;
		this.company = company;
		this.trip_total = trip_total;
		this.trip_seconds = trip_seconds;
		this.trip_miles = trip_miles;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Retorna el id del taxi.
	 * @return taxi_id
	 */
	public String getTaxi_id() {
		return taxi_id;
	}

	/**
	 * Retorna la compa��a del taxi.
	 * @return company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * Retorna el costo total del taxi.
	 * @return trip_total
	 */
	public double getTrip_total() {
		return trip_total;
	}

	/**
	 * Retorna la duraci�n del servicio.
	 * @return trip_seconds
	 */
	public int getTrip_seconds() {
		return trip_seconds;
	}

	/**
	 * Retorna la distancia que recorri� el servicio.
	 * @return trip_miles
	 */
	public double getTrip_miles() {
		return trip_miles;
	}

}
