package view;

import java.util.Iterator;
import java.util.Scanner;

import controller.Controlador;
import model.logic.Chicago;

public class Vista {

	public static void main(String[] args) {
		// Creaci�n del escaner.
		Scanner sc = new Scanner(System.in);

		// Boolean que representa el estado del programa.
		boolean activo = true;

		// Boolean que representa si ya hay informaci�n cargada en el sistema.
		boolean datosCargados = false;

		// Ciclo del programa mientras est� activo.
		while (activo) {

			// Ejecuci�n de la interfaz principal del men�.
			menu();

			// Ejecuta si los datos no han sido cargados.
			if (!datosCargados) {

				// Impresi�n del men� cuando no se han cargado datos.
				menuSinDatos();

				// Lectura de la opci�n que n�mero ingres�.
				int opcion = sc.nextInt();
				String json = "";

				// Switch para saber qu� json eligi�.
				switch (opcion) {

				// El usuario eligi� el archivo peque�o.
				case 1:
					json = Chicago.DIRECCION_SMALL_JSON;
					break;

					// El usuario eligi� el archivo mediano.
				case 2:
					json = Chicago.DIRECCION_MEDIUM_JSON;
					break;

					// El usuario eligi� el archivo grande.
				case 3:
					json = Chicago.DIRECCION_LARGE_JSON;
					break;

					// El usuario eligi� un comando inv�lido.
				default:
					System.out.println("Ingres� un n�mero inv�lido.");
				}

				// L�nea que se ejecuta si el comando que ingres� el usuario es
				// v�lido.
				if (!json.equals("")) {

					// Los datos fueron exitosamente cargados.
					datosCargados = true;

					// Memoria y tiempo antes de cargar el archivo.
					long memoriaInicio = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long tiempoInicio = System.nanoTime();

					// Carga de los datos.
					try {
						Controlador.cargar(json);
					} catch (Exception e) {
						System.out.println(e);
						break;
					}

					// Tiempo que se demor� cargando los datos.
					long tiempoFin = System.nanoTime();
					long duracion = (tiempoFin - tiempoInicio) / (1000000);

					// Memoria usada cargando los datos.
					long memoriaFin = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

					// Mensaje de cu�nto tiempo se gast� durante el m�todo y
					// cu�nta memoria gast�.
					System.out.println("Tiempo que demor� en cargar: " + duracion
							+ " milisegundos \nMemoria utilizada durante la carga:  "
							+ ((memoriaFin - memoriaInicio) / 1000000.0) + " MB");

				}
			}

			// Ejecuta si los datos ya fueron cargados.
			else {

				// Impresi�n del men� cuando se cargaron los datos.
				menuConDatos();

				// Lectura de la opci�n que n�mero ingres�.
				int opcion = sc.nextInt();

				// Switch para saber qu� json eligi�.
				switch (opcion) {

				// El usuario eligi� la opci�n de salir.
				case 1:
					System.out.println("Hasta luego.");
					activo = false;
					sc.close();
					break;

					// El usuario eligi� la opci�n de cargar un nuevo archivo.
				case 2:
					datosCargados = false;
					break;

				case 3:
					System.out.println("El total de taxis en el �rbol es: " + Controlador.getCantidadTaxis());
					break;

				case 4:
					System.out.println("La altura total del �rbol es: " + Controlador.getAlturaArbol());
					break;

				case 5:
					System.out.println("La altura promedio del �rbol es: " + Controlador.getAlturaPromedio());
					break;

				case 6:
					System.out.println("Inserte el id del taxi a buscar");
					String id = sc.next();
					try {
						System.out.println(Controlador.getInformacionTaxi(id));
					} catch(Exception e) {
						System.out.println("No existe el taxi.");
					}
					break;

				case 7:
					System.out.println("Inserte el id inferior");
					String llaveInferior = sc.next();
					System.out.println("Inserte el id superior");
					String llaveSuperior = sc.next();
					
					System.out.println("Llaves entre ellos:");
					int contador = 0;
					Iterator<String> keys = Controlador.getKeysInRange(llaveInferior, llaveSuperior).iterator();
					while(keys.hasNext()) {
						System.out.println(++contador + ". " + keys.next());
					}
					break;
				case 8:
					System.out.println("La altura maxima que se puede obtener del arbol rojo-negro se da por 2lg(n)  (logaritmo en base 2)");
					System.out.println("Se aproximo al entero mayor mas cercano");
					double ans = Math.log((double) Controlador.getAlturaArbol()+1)/Math.log((double)2);
					System.out.println(Math.ceil(2*ans));
					break;
				case 9:
					System.out.println("La altura maxima que se puede obtener del arbol 2-3 se da por lg(n)  (logaritmo en base 3)");
					double ans2 = Math.log((double) Controlador.getAlturaArbol())/Math.log((double)3);
					System.out.println("Se aproximo al entero mayor mas cercano");
					System.out.println(Math.ceil(ans2));
					break;
				case 10:
					System.out.println("La altura minima que se puede obtener del arbol 2-3 se da por 2lg(n)  (logaritmo en base 2)");
					double ans3 = Math.log((double) Controlador.getAlturaArbol())/Math.log((double)3);
					System.out.println("Se aproximo al entero mayor mas cercano");
					System.out.println(Math.ceil(2*ans3));
					break;
				case 11:
					System.out.println("Las ventajas que presenta la implementacion de un arbol rojo negro a uno dos tres es la "
							+ "manera en la cual los datos se ubica, es mas facil hacer las busquedas dentro de este y ademas"+
							"se entiende de manera mas facil");
				default:
					opcion = -1;
					System.out.println("Ingres� un n�mero inv�lido.");
					break;
				}
			}
		}
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos
	// --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo que imprime el men� de la aplicaci�n
	 */
	private static void menu() {
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("------------------------- Taxis de Chicago. Versi�n 2.0 ----------------------------");
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("");
	}

	/**
	 * M�todo que imprime el resto del men� si la aplicaci�n tiene datos
	 * cargados.
	 */
	private static void menuConDatos() {
		System.out.println("1. Salir de la app.");
		System.out.println("2. Cargar nuevo archivo.");
		System.out.println("3. Total de taxis.");
		System.out.println("4. Altura del �rbol.");
		System.out.println("5. Altura promedio del �rbol.");
		System.out.println("6. Informaci�n de un taxi asociada con su id.");
		System.out.println("7. Llaves del �rbol seg�n un rango");
		System.out.println("8. Maxima altura teorica de un arbol rojo-negro");
		System.out.println("9. Minima altura teorica de un arbol 2-3");
		System.out.println("10. Maxima altura teorica de un arbol 2-3");
		System.out.println("11. Comentario");
	}

	/**
	 * M�todo que imprime el resto del men� si la aplicaci�n no tiene datos
	 * cargados.
	 */
	private static void menuSinDatos() {
		System.out.println("Seleccione el archivo que quiere cargar:");
		System.out.println(" 1. Peque�o");
		System.out.println(" 2. Mediano");
		System.out.println(" 3. Grande");
	}
}
