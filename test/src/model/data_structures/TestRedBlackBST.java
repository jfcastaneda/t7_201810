package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Test;

/**
 * Test del IRedBlackBST
 */
public class TestRedBlackBST {
	
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	
	/**
	 * RedBlackBST sobre el cual se ejecutaran las pruebas.
	 */
	private IRedBlackBST<Integer, String> arbol;
	
	// -----------------------------------------------------------------
    // Escenarios
    // -----------------------------------------------------------------
	
    /**
     * Se instancia el arbol. Este m�todo se ejecuta antes de cada m�todo de prueba.
     */
    public void setupEscenario1( )
    {
    	arbol = new RedBlackBST<Integer, String>();
    }
    
    /**
     * Se instancia el arbol y se agrega un elemento.
     */
    public void setupEscenario2() {
    	setupEscenario1();
    	arbol.put(1, "Puto el que lo lea");
    }
    
    /**
     * Se instancia un arbol y se agregan dos elemntos.
     */
    public void setupEscenario3() {
    	setupEscenario2();
    	arbol.put(2, "Gg izi, nubs");
    }
    
    /**
     * Test del m�todo isEmpty().
     */
    @Test
    public void testIsEmpty() {
    	setupEscenario1();
    	assertTrue("El m�todo isEmpty() est� defectuoso.", arbol.isEmpty());
    }
    
    /**
     * Test del m�todo put().
     */
    @Test
    public void testPutIsEmpty() {
    	setupEscenario2();
    	assertTrue("El m�todo agregar est� defectuoso", !arbol.isEmpty());
    }
    
    /**
     * Test del m�todo size().
     */
    @Test
    public void testSize() {
    	setupEscenario2();
    	arbol.put(2, "Gg izi, nubs");
    	assertTrue("El m�todo size no cuantifica bien el tama�o", arbol.size() == 2);
    }
    
    /**
     * Test del metodo get().
     */
    @Test
    public void testGet() {
    	setupEscenario2();
    	assertEquals("El metodo get esta defectuoso", "Puto el que lo lea", arbol.get(1));
    }
    
    /**
     * Test del metodo getHeight().
     */
    @Test
    public void testGetHeight() {
    	setupEscenario3();
    	arbol.put(3, "Gg izi, nubs");
    	arbol.put(4, "Gg izi, nubs");
    	assertTrue("El metodo getHeight esta defectuoso", arbol.getHeight(3) == 0);
    }
    
    /**
     * Test del metodo contains().
     */
    @Test
    public void testContains() {
    	setupEscenario2();
    	assertTrue("El metodo getHeight esta defectuoso", !arbol.contains(2));
    	assertTrue("El metodo getHeight esta defectuoso", arbol.contains(1));
    }
    
    /**
     * Test del metodo height().
     */
    @Test
    public void testHeight() {
    	setupEscenario3();
    	arbol.put(3, "Gg izi, nubs");
    	arbol.put(4, "Gg izi, nubs");
    	assertTrue("El metodo height esta defectuoso", arbol.height() == 2);
    }
    
    /**
     * Test del metodo max().
     */
    @Test
    public void max() {
    	setupEscenario3();
    	assertTrue("El metodo max esta defectuoso", arbol.max() == 2);
    }
    
    /**
     * Test del metodo min().
     */
    @Test
    public void min() {
    	setupEscenario3();
    	assertTrue("El metodo min esta defectuoso", arbol.min() == 1);
    }
    
    /**
     * Test del metodo keys().
     */
    @Test
    public void testKeys() {
    	setupEscenario3();
    	Iterator<Integer> iter = arbol.keys().iterator();
    	assertTrue("El metodo keys esta defectuoso", iter.next() == 1);
    	assertTrue("El metodo keys esta defectuoso", iter.next() == 2);
    }
    
    /**
     * Test del metodo keysInRange().
     */
    @Test 
    public void testKeysInRange() {
    	setupEscenario3();
    	Iterator<Integer> iter = arbol.keysInRange(2, 10).iterator();
    	assertTrue("El metodo keysInRange est� defectuoso", iter.next() == 2);
    	assertTrue("El m�todo keysInRange est� defectuoso", !iter.hasNext());
    }
}
